import React from 'react'
import { render } from 'react-dom'
import 'reset.css'
import 'flex-layouts/lib/flex-layouts.css'
import configureStore from './app/store/configureStore'
import Root from './app/containers/Root'
import './index.css'

const store = configureStore()

render(
  <Root store={store} />,
  document.getElementById('container'),
)

if (module.hot) {
  module.hot.accept('./app/containers/Root', () => {
    const HotRoot = require('./app/containers/Root').default // eslint-disable-line global-require

    render(
      <HotRoot store={store} />,
      document.getElementById('container'),
    )
  })
}
