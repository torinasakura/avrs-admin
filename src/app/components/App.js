import React, { PropTypes } from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Link } from 'react-router'
import { NavLink } from 'avrs-ui/src/link'
import { Divider } from 'avrs-ui/src/divider'
import { Item } from 'avrs-ui/src/navigation'
import { LogOutIcon } from 'avrs-ui/src/icons'
import { Logo } from 'avrs-ui/src/logo'
import Workspace from './Workspace'

const App = ({ children, onLogout }) => (
  <Row fill>
    <Layout>
      <Column align='center'>
        <Layout basis='25px' />
        <Layout align='center'>
          <Link to='/'>
            <Logo height={24} />
          </Link>
        </Layout>
        <Layout basis='30px' />
        <Layout>
          <Divider />
        </Layout>
        <Layout>
          <NavLink to='/support/external'>
            Обращения
          </NavLink>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <NavLink to='/users'>
            Пользователи
          </NavLink>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <NavLink to='/operations'>
            Операции
          </NavLink>
        </Layout>
        <Layout grow={1} />
        <Layout>
          <Divider />
        </Layout>
        <Layout>
          <Column>
            <Layout basis='1px'>
              <Divider vertical />
            </Layout>
            <Layout>
              <Item
                plain
                onClick={onLogout}
              >
                <LogOutIcon />
              </Item>
            </Layout>
          </Column>
        </Layout>
      </Column>
    </Layout>
    <Layout shrink={1} grow={1}>
      <Workspace>
        {children}
      </Workspace>
    </Layout>
  </Row>
)

App.propTypes = {
  children: PropTypes.element,
}

export default App
