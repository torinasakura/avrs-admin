import React, { PropTypes } from 'react'
import { StyleSheet } from 'elementum'
import { Column, Row, Layout } from 'flex-layouts'

const styles = StyleSheet.create({
  self: {
    width: '100%',
    overflowY: 'auto',
    background: '#f3f5f8',
    boxSizing: 'border-box',
    boxShadow: 'inset 0px 1px 3px 0px rgba(29, 25, 42, 0.15)',
    display: 'flex',
    flexDirection: 'column',
  },
})

const Workspace = ({ children }) => (
  <div className={styles()}>
    <div style={{ display: 'block' }}>
      <Column>
        <Layout basis='20px' />
        <Layout shrink={1} grow={1}>
          <Row>
            <Layout basis='20px' />
            <Layout>
              {children}
            </Layout>
            <Layout basis='20px' />
          </Row>
        </Layout>
        <Layout basis='20px' />
      </Column>
    </div>
  </div>
)

Workspace.propTypes = {
  children: PropTypes.element,
}

export default Workspace
