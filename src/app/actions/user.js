import gql from 'graphql-tag'
import * as actions from '../constants/user'

export function auth(user) {
  return async (dispatch) => {
    dispatch({
      type: actions.auth,
      user,
    })

    window.location.hash = '/'
  }
}

export function logout() {
  return async (dispatch, getState) => {
    if (getState().user.token) {
      dispatch({
        type: actions.logout,
      })
    }

    if (getState().router.location.pathname.indexOf('/auth') !== 0) {
      window.location.hash = '/auth/login'
    }
  }
}

export function load(forceFetch = false) {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      forceFetch,
      query: gql`
        query {
          user {
            id
            email
            firstName
            lastName
            balance
            inviteCode
            plan {
              id,
              type,
              name,
              time,
              price,
              period,
              profitability,
              profitabilityPerDay,
              profitabilityPerHour,
              profit,
              amount,
              memory,
              cpu {
                from,
                to
              },
              expireAt
            }
            activations {
              startAt
              expireAt
              servicePlan {
                name
              }
            }
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      user: data.user,
    })
  }
}
