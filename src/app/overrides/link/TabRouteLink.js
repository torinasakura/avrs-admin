/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
import { TabRouteLink } from 'avrs-ui/src/OriginalLink'
import OverrideRouteLink from './OverrideRouteLink'

export default OverrideRouteLink(TabRouteLink)
