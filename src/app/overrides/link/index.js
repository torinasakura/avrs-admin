/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
import { Link, PointerLink, NavLink } from 'avrs-ui/src/OriginalLink'
import RouteLink from './RouteLink'
import TabRouteLink from './TabRouteLink'

export {
  Link,
  PointerLink,
  RouteLink,
  TabRouteLink,
  NavLink,
}
