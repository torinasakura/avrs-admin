/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
import { RouteLink } from 'avrs-ui/src/OriginalLink'
import OverrideRouteLink from './OverrideRouteLink'

export default OverrideRouteLink(RouteLink)
