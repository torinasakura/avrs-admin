import React from 'react'
import { connect } from 'react-redux'
import { routerShape } from 'react-router/lib/PropTypes'

const overrides = {
  '^/network/top$': ({ userId }, to) => `/users/${userId}${to}`,
  '^/network/direct$': ({ userId }, to) => `/users/${userId}${to}`,
  '^/network/tree$': ({ userId }, to) => `/users/${userId}${to}`,
  '^/network/tree/\\d+/stat$': ({ userId }, to) => `/users/${userId}${to}`,
  '^/money/rent$': ({ userId }, to) => `/users/${userId}${to}`,
  '^/money/referal': ({ userId }, to) => `/users/${userId}${to}`,
  '^/money/operations_history': ({ userId }, to) => `/users/${userId}${to}`,
}

let listener = null

const replaceRoutes = (to, { params }, { router }) => {
  if (!listener) {
    listener = router.listenBefore((location) => {
      const [matched] = Object.keys(overrides).filter(key => (new RegExp(key)).test(location.pathname))

      if (matched) {
        router.push(overrides[matched](params, location.pathname))
      }
    })
  }

  const [matched] = Object.keys(overrides).filter(key => (new RegExp(key)).test(to))

  if (matched) {
    return overrides[matched](params, to)
  }

  return to
}

const OverrideRouteLink = (Target) => {
  const Override = ({ to, router, ...props }, context) => (
    <Target
      {...props}
      to={replaceRoutes(to, router, context)}
    />
  )

  Override.contextTypes = {
    router: routerShape,
  }

  return connect(
    state => ({
      router: state.router,
    }),
  )(Override)
}

export default OverrideRouteLink
