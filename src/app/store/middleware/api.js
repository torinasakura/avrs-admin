export default (client, authClient) => ({ dispatch, getState }) => next => (action) => {
  if (typeof action === 'function') {
    action(dispatch, getState, client, authClient)
  } else {
    next(action)
  }
}
