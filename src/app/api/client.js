import ApolloClient, { createNetworkInterface } from 'apollo-client'
import { start, end } from '../actions/remote'
import { logout } from '../actions/user'

const networkInterface = createNetworkInterface({ uri: `${process.env.API_URL}admin` })

const client = new ApolloClient({
  networkInterface,
})

networkInterface.use([{
  applyMiddleware(req, next) {
    const user = client.store.getState().user

    if (!req.options.headers) {
      req.options.headers = {} // eslint-disable-line no-param-reassign
    }

    if (user && user.token) {
      req.options.headers.authorization = user.token // eslint-disable-line no-param-reassign
    }

    client.store.dispatch(start())

    next()
  },
}])

networkInterface.useAfter([{
  applyAfterware({ response }, next) {
    client.store.dispatch(end())

    if (response.status === 401) {
      client.store.dispatch(logout())
    }

    next()
  },
}])

export default client
