import ApolloClient, { createNetworkInterface } from 'apollo-client'

const networkInterface = createNetworkInterface({ uri: process.env.API_URL })

const authClient = new ApolloClient({
  networkInterface,
})

export default authClient
