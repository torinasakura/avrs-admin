import App from '../containers/App'
import Container from '../containers/Container'
import home from '../pages/home/routes'
import auth from '../pages/auth/routes'
import support from '../pages/support/routes'
import operations from '../pages/operations/routes'
import users from '../pages/users/routes'

export default function getRoutes(store) {
  return {
    component: Container,
    childRoutes: [
      auth(store),
      {
        path: '/',
        component: App,
        indexRoute: home(store),
        childRoutes: [
          ...support(store),
          ...operations(store),
          ...users(store),
        ],
      },
    ],
  }
}
