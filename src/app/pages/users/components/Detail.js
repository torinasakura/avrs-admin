import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { Condition } from 'avrs-ui/src/condition'
import { TabRouteLink, Link } from 'avrs-ui/src/link'
import { Text } from 'avrs-ui/src/text'

const Detail = ({ children, id, isNew, isAdmin, onAddAdminPermission, onRemoveAdminPermission }) => (
  <Column>
    <Layout grow={1} />
    <Layout basis='924px'>
      <Row>
        <Layout>
          <Block shadow>
            <Column align='center'>
              <Layout>
                <TabRouteLink to={`/users/${id}/profile`}>
                  Профайл
                </TabRouteLink>
              </Layout>
              <Condition match={!(isNew || isAdmin)}>
                <Layout>
                  <TabRouteLink to={`/users/${id}/home`}>
                    Главная
                  </TabRouteLink>
                </Layout>
              </Condition>
              <Condition match={!(isNew || isAdmin)}>
                <Layout>
                  <TabRouteLink to={`/users/${id}/service_plans`}>
                    Тарифы
                  </TabRouteLink>
                </Layout>
              </Condition>
              <Condition match={!(isNew || isAdmin)}>
                <Layout>
                  <TabRouteLink to={`/users/${id}/money`}>
                    Деньги
                  </TabRouteLink>
                </Layout>
              </Condition>
              <Condition match={!(isNew || isAdmin)}>
                <Layout>
                  <TabRouteLink to={`/users/${id}/network`}>
                    Сеть
                  </TabRouteLink>
                </Layout>
              </Condition>
              <Layout grow={1} />
              <Layout>
                <Condition match={!isAdmin}>
                  <Link onClick={onAddAdminPermission}>
                    <Text
                      color='blue400'
                      size='xsmall'
                    >
                      Выдать права админа
                    </Text>
                  </Link>
                </Condition>
                <Condition match={isAdmin}>
                  <Link onClick={onRemoveAdminPermission}>
                    <Text
                      color='blue400'
                      size='xsmall'
                    >
                      Удалить права админа
                    </Text>
                  </Link>
                </Condition>
              </Layout>
              <Layout basis='25px' />
            </Column>
          </Block>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          {children}
        </Layout>
      </Row>
    </Layout>
    <Layout grow={1} />
  </Column>
)

export default Detail
