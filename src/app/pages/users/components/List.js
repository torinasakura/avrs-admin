import React from 'react'
import { Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { Table, Row as TableRow, Cell } from 'avrs-ui/src/table'
import { RouteLink } from 'avrs-ui/src/link'
import { Text } from 'avrs-ui/src/text'

const statuses = {
  NEW: 'Новый',
  ACTIVE: 'Активный',
  INACTIVE: 'Неактивный',
}

const List = ({ users }) => (
  <Block
    offset
    shadow
  >
    <Row>
      <Layout>
        <Table>
          <TableRow>
            <Cell>
              ID
            </Cell>
            <Cell>
              Email
            </Cell>
            <Cell>
              Имя Фамилия
            </Cell>
            <Cell>
              Статус
            </Cell>
            <Cell>
              Админ
            </Cell>
          </TableRow>
          {users.map((user, index) => (
            <TableRow key={index}>
              <Cell>
                {user.id}
              </Cell>
              <Cell>
                <RouteLink to={`/users/${user.id}`}>
                  <Text size='small' color='blue400'>
                    {user.email}
                  </Text>
                </RouteLink>
              </Cell>
              <Cell>
                {user.firstName} {user.lastName}
              </Cell>
              <Cell>
                {statuses[user.status]}
              </Cell>
              <Cell>
                {user.isAdmin ? 'Да' : 'Нет'}
              </Cell>
            </TableRow>
          ))}
        </Table>
      </Layout>
    </Row>
  </Block>
)

export default List
