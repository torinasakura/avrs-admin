import { combineReducers } from 'redux'
import list from './list'
import detail from './detail'
import network from './network'
import home from './home'
import money from './money'

export default combineReducers({
  list,
  detail,
  network,
  home,
  money,
})
