import moment from 'moment'
import { getPackage } from 'avrs-cabinet/src/app/pages/money/reducers/utils'
import { createReducer } from '../../../../../utils'
import * as actions from '../../constants/money/referal'

const initialState = {
  operations: [],
}

export default createReducer(initialState, {
  [actions.load]: (state, { operations }) => ({
    ...state,
    operations: operations.map(operation => ({
      ...operation,
      percent: operation.percent * 100,
      package: getPackage(operation.package),
      date: moment(new Date(operation.date)).format('YYYY-MM-DD'),
      participant: `${operation.participant.firstName} ${operation.participant.lastName}`,
    })),
  }),
})
