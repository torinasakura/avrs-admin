import moment from 'moment'
import { formatActivation, sortActivations } from 'avrs-cabinet/src/app/reducers/user'
import { createReducer } from '../../../../utils'
import * as actions from '../constants/detail'

const initialState = {}

export default createReducer(initialState, {
  [actions.load]: (state, { user }) => ({
    ...state,
    ...user,
    isNew: user.status === 'NEW',
    notActivated: user.status === 'NOT_ACTIVATED',
    refLink: `${window.location.host}/auth/registration?rf=${user.inviteCode}`,
    registeredAt: moment(new Date(user.createdAt)).format('YYYY/MM/DD'),
    activations: (user.activations || []).sort(sortActivations).map(formatActivation),
  }),
})
