import { createReducer } from '../../../../utils'
import * as actions from '../constants/list'

const initialState = []

export default createReducer(initialState, {
  [actions.load]: (state, { users }) => users,
})
