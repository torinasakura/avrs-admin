import { load as loadList } from '../actions/list'
import { load as loadDetail } from '../actions/detail'
import { loadStat } from '../actions/home'
import { load as loadDirect } from '../actions/network/direct'
import { load as loadTop } from '../actions/network/top'
import { load as loadHierarchy, loadReferal } from '../actions/network/hierarchy'
import { load as loadRentalOperations } from '../actions/money/rental'
import { load as loadReferalOperations } from '../actions/money/referal'
import { load as loadHistoryOperations } from '../actions/money/history'
import List from '../containers/List'
import Detail from '../containers/Detail'
import Home from '../containers/home/Home'
import ServicePlans from '../containers/ServicePlans'
import Money from '../containers/money/Money'
import RentalOperations from '../containers/money/Rental'
import ReferalOperations from '../containers/money/Referal'
import OperationsHistory from '../containers/money/OperationsHistory'
import Profile from '../containers/profile/Profile'
import GeneralInformation from '../containers/profile/GeneralInformation'
import Network from '../containers/network/Network'
import Top from '../containers/network/Top'
import Direct from '../containers/network/Direct'
import Hierarchy from '../containers/network/Hierarchy'
import Referal from '../containers/network/referal/Referal'
import Stat from '../containers/network/referal/Stat'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'users',
    component: List,
    onEnter() {
      dispatch(loadList())
    },
  }, {
    path: 'users/:userId',
    component: Detail,
    onEnter({ location, params }, replace) {
      dispatch(loadDetail(params.userId))

      if (location.pathname === `/users/${params.userId}`) {
        replace(`/users/${params.userId}/profile`)
      }
    },
    childRoutes: [{
      path: 'profile',
      component: Profile,
      onEnter({ location, params }, replace) {
        if (location.pathname === `/users/${params.userId}/profile`) {
          replace(`/users/${params.userId}/profile/general_information`)
        }
      },
      childRoutes: [
        {
          path: 'general_information',
          component: GeneralInformation,
        },
      ],
    }, {
      path: 'home',
      component: Home,
      onEnter({ params }) {
        dispatch(loadStat(params.userId))
      },
    }, {
      path: 'service_plans',
      component: ServicePlans,
    }, {
      path: 'money',
      component: Money,
      onEnter({ location, params }, replace) {
        if (location.pathname === `/users/${params.userId}/money`) {
          replace(`/users/${params.userId}/money/rent`)
        }
      },
      childRoutes: [{
        path: 'rent',
        component: RentalOperations,
        onEnter({ params }) {
          dispatch(loadRentalOperations(params.userId))
        },
      }, {
        path: 'referal',
        component: ReferalOperations,
        onEnter({ params }) {
          dispatch(loadReferalOperations(params.userId))
        },
      }, {
        path: 'operations_history',
        component: OperationsHistory,
        onEnter({ params }) {
          dispatch(loadHistoryOperations(params.userId))
        },
      }],
    }, {
      path: 'network',
      component: Network,
      onEnter({ location, params }, replace) {
        if (location.pathname === `/users/${params.userId}/network`) {
          replace(`/users/${params.userId}/network/top`)
        }
      },
      childRoutes: [{
        path: 'top',
        component: Top,
        onEnter({ params }) {
          dispatch(loadTop(params.userId))
        },
      }, {
        path: 'direct',
        component: Direct,
        onEnter({ params }) {
          dispatch(loadDirect(params.userId))
        },
      }, {
        path: 'tree',
        component: Hierarchy,
        onEnter({ params }) {
          dispatch(loadHierarchy(params.userId))
        },
        childRoutes: [{
          path: ':referalId',
          component: Referal,
          onEnter({ params }) {
            dispatch(loadReferal(params.referalId))
          },
          childRoutes: [{
            path: 'stat',
            component: Stat,
          }],
        }],
      }],
    }],
  }]
}
