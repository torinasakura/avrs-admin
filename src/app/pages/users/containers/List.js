import { connect } from 'react-redux'
import List from '../components/List'

export default connect(
  state => ({
    users: state.users.list,
  }),
)(List)
