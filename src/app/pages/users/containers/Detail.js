import { connect } from 'react-redux'
import { addAdminPermission, removeAdminPermission } from '../actions/detail'
import Detail from '../components/Detail'

export default connect(
  state => state.users.detail,
  dispatch => ({
    onAddAdminPermission: () => dispatch(addAdminPermission()),
    onRemoveAdminPermission: () => dispatch(removeAdminPermission()),
  }),
)(Detail)
