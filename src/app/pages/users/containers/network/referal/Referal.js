import { connect } from 'react-redux'
import Referal from 'avrs-cabinet/src/app/pages/network/components/referal/Referal'

export default connect(
  state => state.users.network.hierarchy.stat || {},
)(Referal)
