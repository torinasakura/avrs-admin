import { connect } from 'react-redux'
import Position from 'avrs-cabinet/src/app/pages/network/components/Position'

export default connect(
  state => ({
    salesBalance: state.users.detail.salesBalance,
  }),
)(Position)
