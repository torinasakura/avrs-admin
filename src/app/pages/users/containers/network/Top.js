import { connect } from 'react-redux'
import Top from 'avrs-cabinet/src/app/pages/network/components/Top'

export default connect(
  state => ({
    users: state.users.network.top.users,
  }),
)(Top)
