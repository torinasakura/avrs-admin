import { connect } from 'react-redux'
import Direct from 'avrs-cabinet/src/app/pages/network/components/Direct'

export default connect(
  state => ({
    users: state.users.network.direct.users,
  }),
)(Direct)
