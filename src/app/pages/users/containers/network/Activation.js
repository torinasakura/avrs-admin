import { connect } from 'react-redux'
import Activation from 'avrs-cabinet/src/app/pages/network/components/Activation'

export default connect(
  state => ({
    ...(state.users.detail.activations ? state.users.detail.activations[0] : { servicePlan: {} }),
  }),
)(Activation)
