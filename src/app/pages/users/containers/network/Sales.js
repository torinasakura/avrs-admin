import { connect } from 'react-redux'
import Sales from 'avrs-cabinet/src/app/pages/network/components/Sales'

export default connect(
  state => ({
    salesBalance: state.users.detail.salesBalance,
  }),
)(Sales)
