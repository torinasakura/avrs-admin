import { connect } from 'react-redux'
import Referals from 'avrs-cabinet/src/app/pages/network/components/Referals'

export default connect(
  state => ({
    referals: state.users.detail.referals,
  }),
)(Referals)
