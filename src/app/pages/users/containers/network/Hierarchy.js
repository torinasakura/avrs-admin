import { connect } from 'react-redux'
import Hierarchy from 'avrs-cabinet/src/app/pages/network/components/Hierarchy'

export default connect(
  state => ({
    data: state.users.network.hierarchy.tree,
  }),
)(Hierarchy)
