import { connect } from 'react-redux'
import Payments from 'avrs-cabinet/src/app/pages/home/components/Payments'

export default connect(
  state => ({
    loaded: state.users.home.payments.loaded,
    data: [state.users.home.payments.rental, state.users.home.payments.referal],
  }),
)(Payments)
