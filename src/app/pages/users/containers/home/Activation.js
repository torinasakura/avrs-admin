import { connect } from 'react-redux'
import Activation from 'avrs-cabinet/src/app/pages/home/components/Activation'

export default connect(
  state => ({
    ...(state.users.detail.activations ? state.users.detail.activations[0] : { servicePlan: {} }),
  }),
)(Activation)
