import { connect } from 'react-redux'
import Network from 'avrs-cabinet/src/app/pages/home/components/Network'

export default connect(
  state => ({
    loaded: state.users.home.network.loaded,
    data: [state.users.home.network.connections, state.users.home.network.activations],
  }),
)(Network)
