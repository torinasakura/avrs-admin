import { connect } from 'react-redux'
import Balance from 'avrs-cabinet/src/app/pages/money/components/Balance'

export default connect(
  state => ({
    balance: state.users.detail.balance,
  }),
)(Balance)
