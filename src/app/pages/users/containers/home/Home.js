import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import Activation from './Activation'
import Balance from './Balance'
import Payments from './Payments'
import Network from './Network'

const Home = () => (
  <Column>
    <Layout grow={1} />
    <Layout basis='924px'>
      <Row>
        <Layout>
          <Column>
            <Layout shrink={1} basis='50%'>
              <Balance />
            </Layout>
            <Layout basis='25px' />
            <Layout shrink={1} basis='50%'>
              <Activation />
            </Layout>
          </Column>
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <Payments />
        </Layout>
        <Layout basis='20px' />
        <Layout>
          <Network />
        </Layout>
      </Row>
    </Layout>
    <Layout grow={1} />
  </Column>
)

export default Home
