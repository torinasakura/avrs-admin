import { connect } from 'react-redux'
import GeneralInformation from 'avrs-cabinet/src/app/pages/profile/components/GeneralInformation'

export default connect(
  state => ({
    ...state.users.detail,
    errors: {},
  }),
)(GeneralInformation)
