import { connect } from 'react-redux'
import Information from 'avrs-cabinet/src/app/pages/profile/components/Information'

export default connect(
  state => state.users.detail,
)(Information)
