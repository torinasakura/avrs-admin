import { connect } from 'react-redux'
import OperationsHistory from 'avrs-cabinet/src/app/pages/money/components/OperationsHistory'

export default connect(
  state => ({
    operations: state.users.money.history.operations,
  }),
)(OperationsHistory)
