import { connect } from 'react-redux'
import ReferalBalance from 'avrs-cabinet/src/app/pages/money/components/ReferalBalance'

export default connect(
  state => ({
    balance: state.users.detail.referalBralance,
  }),
)(ReferalBalance)
