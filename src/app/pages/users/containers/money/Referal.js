import { connect } from 'react-redux'
import Referal from 'avrs-cabinet/src/app/pages/money/components/Referal'

export default connect(
  state => ({
    operations: state.users.money.referal.operations,
  }),
)(Referal)
