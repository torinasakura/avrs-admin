import { connect } from 'react-redux'
import Rental from 'avrs-cabinet/src/app/pages/money/components/Rental'

export default connect(
  state => ({
    operations: state.users.money.rental.operations,
  }),
)(Rental)
