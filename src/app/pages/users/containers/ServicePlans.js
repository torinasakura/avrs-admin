import React from 'react'
import { connect } from 'react-redux'
import { Column, Layout } from 'flex-layouts'
import Activations from 'avrs-cabinet/src/app/pages/ServicePlans/components/Activations'

const ServicePlans = props => (
  <Column>
    <Layout grow={1} />
    <Layout basis='924px'>
      <Activations {...props} />
    </Layout>
    <Layout grow={1} />
  </Column>
)

export default connect(
  state => ({
    activations: state.users.detail.activations || [],
  }),
  () => ({
    onStart: f => f,
    onStop: f => f,
  }),
)(ServicePlans)
