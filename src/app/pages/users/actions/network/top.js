import gql from 'graphql-tag'
import * as actions from '../../constants/network/top'

export function load(id) {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          networkTopReferals (id: "${id}") {
            id
            firstName
            lastName
            salesBalance
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      users: data.networkTopReferals,
    })
  }
}
