import gql from 'graphql-tag'
import * as actions from '../../constants/network/direct'

export function load(id) {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          networkDirectReferals (id: "${id}") {
            id
            email
            firstName
            lastName
            createdAt
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      users: data.networkDirectReferals,
    })
  }
}
