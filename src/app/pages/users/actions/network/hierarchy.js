import gql from 'graphql-tag'
import * as actions from '../../constants/network/hierarchy'

export function load(id) {
  return async (dispatch, getState, client) => {
    const user = getState().user

    const { data } = await client.query({
      query: gql`
        query {
          networkHierarchy (id: "${id}") {
            id
            firstName
            lastName
            children {
              id
              firstName
              lastName
            }
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      current: user,
      children: data.networkHierarchy,
    })
  }
}

export function loadReferal(id) {
  return async (dispatch, getState, client) => {
    if (id === getState().users.detail.id) {
      return
    }

    const { data } = await client.query({
      query: gql`
        query {
          networkReferalStat (id: "${id}") {
            id
            firstName
            lastName
            salesBalance
          }
        }
      `,
    })

    dispatch({
      type: actions.loadStat,
      stat: data.networkReferalStat,
    })
  }
}
