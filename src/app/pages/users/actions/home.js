import gql from 'graphql-tag'
import * as actions from '../constants/home'

export function loadStat(id) {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          paymentsStat (id: ${id}) {
            rental {
              date
              amount
            }
            referal {
              date
              amount
            }
          }
          networkStat (id: ${id}) {
            connections {
              date
              amount
            }
            activations {
              date
              amount
            }
          }
        }
      `,
    })

    dispatch({
      type: actions.loadPayments,
      stat: data.paymentsStat,
    })

    dispatch({
      type: actions.loadNetwork,
      stat: data.networkStat,
    })
  }
}
