import gql from 'graphql-tag'
import * as actions from '../constants/detail'

export function load(id, forceFetch = false) {
  return async (dispatch, getState, client) => {
    const [user] = getState().users.list.filter(item => item.id === id)

    if (user) {
      dispatch({
        type: actions.load,
        user,
      })
    }

    const { data } = await client.query({
      forceFetch,
      query: gql`
        query {
          user(id: "${id}") {
            id
            isAdmin
            email
            firstName
            lastName
            inviteCode
            status
            balance
            salesBalance
            referals
            country
            sex
            phone
            birthday
            sponsor {
              id
              firstName
              lastName
            }
            activations {
              id
              status
              startAt
              leftTime
              servicePlan {
                type
                period
                time
                price
                profitabilityPerDay
                profitabilityPerHour
                amount
                memory
                cpu
              }
            }
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      user: data.user,
    })
  }
}

export function addAdminPermission() {
  return async (dispatch, getState, client) => {
    const { id } = getState().users.detail

    const { data } = await client.mutate({
      mutation: gql`
        mutation addAdminPermission($id: ID!) {
          addAdminPermission(id: $id) {
            id
          }
        }
      `,
      variables: {
        id,
      },
    })

    if (data.addAdminPermission.id) {
      dispatch(load(data.addAdminPermission.id, true))
    }
  }
}

export function removeAdminPermission() {
  return async (dispatch, getState, client) => {
    const { id } = getState().users.detail

    const { data } = await client.mutate({
      mutation: gql`
        mutation removeAdminPermission($id: ID!) {
          removeAdminPermission(id: $id) {
            id
          }
        }
      `,
      variables: {
        id,
      },
    })

    if (data.removeAdminPermission.id) {
      dispatch(load(data.removeAdminPermission.id, true))
    }
  }
}
