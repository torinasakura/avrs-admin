import gql from 'graphql-tag'
import * as actions from '../../constants/money/rental'

export function load(id) {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          rentalOperations (id: ${id}) {
            id
            date
            amount
            package
            time
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      operations: data.rentalOperations,
    })
  }
}
