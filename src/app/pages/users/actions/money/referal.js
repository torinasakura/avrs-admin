import gql from 'graphql-tag'
import * as actions from '../../constants/money/referal'

export function load(id) {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          referalOperations (id: ${id}) {
            id
            date
            amount
            package
            percent
            participant {
              id
              firstName
              lastName
            }
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      operations: data.referalOperations,
    })
  }
}
