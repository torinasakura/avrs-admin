import gql from 'graphql-tag'
import * as actions from '../../constants/money/history'

export function load(id, forceFetch) {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      forceFetch,
      query: gql`
        query {
          operationsHistory (id: ${id}) {
            id
            date
            amount
            direction
            status
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      operations: data.operationsHistory,
    })
  }
}
