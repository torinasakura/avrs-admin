import gql from 'graphql-tag'
import * as actions from '../constants/list'

export function load() {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          users {
            id
            email
            firstName
            lastName
            status
            isAdmin
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      users: data.users,
    })
  }
}
