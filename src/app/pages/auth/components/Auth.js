import React, { PropTypes } from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Logo } from 'avrs-ui/src/logo'
import { BackgroundArrowSection } from 'avrs-ui/src/section'
import { Divider } from 'avrs-ui/src/divider'

const Auth = ({ children }) => (
  <Row fill>
    <Layout basis='10px' />
    <Layout>
      <Column align='center'>
        <Layout basis='35px' />
        <Layout>
          <Logo height={25} />
        </Layout>
      </Column>
    </Layout>
    <Layout basis='10px' />
    <Layout>
      <Divider />
    </Layout>
    <Layout grow={1}>
      <BackgroundArrowSection>
        <Row align='center' justify='center' fill>
          <Layout>
            <div style={{ width: 480 }}>
              {children}
            </div>
          </Layout>
        </Row>
      </BackgroundArrowSection>
    </Layout>
  </Row>
)

Auth.propTypes = {
  children: PropTypes.element,
}

export default Auth

