import { logout } from '../../../actions/user'
import Auth from '../containers/Auth'
import Login from '../containers/Login'

export default function getRoutes({ dispatch }) {
  return {
    component: Auth,
    path: 'auth',
    childRoutes: [
      {
        path: 'login',
        component: Login,
      },
      {
        path: 'logout',
        onEnter() {
          dispatch(logout())
        },
      },
    ],
  }
}
