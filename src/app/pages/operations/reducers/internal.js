import { createReducer } from '../../../../utils'
import * as actions from '../constants'

const initialState = []

export default createReducer(initialState, {
  [actions.load]: (state, { operations }) => operations.filter(operation => operation.direction === 'INTERNAL'),
  [actions.update]: (state, { operation }) => state.map((item) => {
    if (item.id === operation.id) {
      return {
        ...item,
        ...operation,
      }
    }

    return item
  }),
})
