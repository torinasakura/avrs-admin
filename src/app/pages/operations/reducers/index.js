import { combineReducers } from 'redux'
import external from './external'
import internal from './internal'

export default combineReducers({
  external,
  internal,
})
