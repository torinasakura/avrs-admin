import gql from 'graphql-tag'
import * as actions from '../constants'

export function load() {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          operations {
            id
            date
            amount
            status
            direction
            externalType
            user {
              id
              firstName
              lastName
              balance
              cardNumber
              btcAddress
            }
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      operations: data.operations,
    })
  }
}

export function confirm() {
  return async (dispatch, getState, client) => {
    const { id } = getState().router.params

    const { data } = await client.mutate({
      mutation: gql`
        mutation confirmOperation($id: ID!) {
          confirmOperation(id: $id) {
            id
            status
          }
        }
      `,
      variables: {
        id,
      },
    })

    dispatch({
      type: actions.update,
      operation: data.confirmOperation,
    })
  }
}

export function cancelInternal() {
  return async (dispatch, getState, client) => {
    const { id } = getState().router.params

    const { data } = await client.mutate({
      mutation: gql`
        mutation cancelInternalOperation($id: ID!) {
          cancelInternalOperation(id: $id) {
            id
            status
            user {
              id
              firstName
              lastName
              balance
              cardNumber
              btcAddress
            }
          }
        }
      `,
      variables: {
        id,
      },
    })

    dispatch({
      type: actions.update,
      operation: data.cancelInternalOperation,
    })
  }
}

export function cancelExternal() {
  return async (dispatch, getState, client) => {
    const { id } = getState().router.params

    const { data } = await client.mutate({
      mutation: gql`
        mutation cancelExternalOperation($id: ID!) {
          cancelExternalOperation(id: $id) {
            id
            status
            user {
              id
              firstName
              lastName
              balance
              cardNumber
              btcAddress
            }
          }
        }
      `,
      variables: {
        id,
      },
    })

    dispatch({
      type: actions.update,
      operation: data.cancelExternalOperation,
    })
  }
}
