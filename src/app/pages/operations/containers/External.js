import { connect } from 'react-redux'
import External from '../components/External'

export default connect(
  state => ({
    operations: state.operations.external,
  }),
)(External)
