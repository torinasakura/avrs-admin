import { connect } from 'react-redux'
import { confirm, cancelExternal } from '../actions'
import ExternalDetail from '../components/ExternalDetail'

export default connect(
  (state) => {
    const { id } = state.router.params
    const [operation] = state.operations.external.filter(item => item.id === id)

    return {
      ...operation,
    }
  },
  (dispatch, { router }) => ({
    goBack: router.goBack,
    onConfirm: () => dispatch(confirm()),
    onCancel: () => dispatch(cancelExternal()),
  }),
)(ExternalDetail)
