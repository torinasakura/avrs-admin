import { connect } from 'react-redux'
import Internal from '../components/Internal'

export default connect(
  state => ({
    operations: state.operations.internal,
  }),
)(Internal)
