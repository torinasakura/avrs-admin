import { connect } from 'react-redux'
import { confirm, cancelInternal } from '../actions'
import InternalDetail from '../components/InternalDetail'

export default connect(
  (state) => {
    const { id } = state.router.params
    const [operation] = state.operations.internal.filter(item => item.id === id)

    return {
      ...operation,
    }
  },
  (dispatch, { router }) => ({
    goBack: router.goBack,
    onConfirm: () => dispatch(confirm()),
    onCancel: () => dispatch(cancelInternal()),
  }),
)(InternalDetail)
