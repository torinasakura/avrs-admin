import React from 'react'
import moment from 'moment'
import { Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { Table, Row as TableRow, Cell } from 'avrs-ui/src/table'
import { RouteLink } from 'avrs-ui/src/link'
import { Text } from 'avrs-ui/src/text'

const statuses = {
  SENT: 'Ожидание',
  PERFORMED: 'Выполнена',
  NOT_PERFORMED: 'Отклонена',
}

const External = ({ children, operations }) => (
  <Block
    offset
    shadow
  >
    <Row>
      <Layout>
        {children}
      </Layout>
      <Layout>
        <Table>
          <TableRow>
            <Cell
              align='center'
              weight='medium'
            >
              #
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Пользователь
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Тип
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Сумма
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Дата
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Баланс
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Статус
            </Cell>
          </TableRow>
          {operations.map((operation, index) => (
            <TableRow key={index}>
              <Cell>
                <RouteLink to={`/operations/external/${operation.id}`}>
                  <Text size='small' color='blue400'>
                    {operation.id}
                  </Text>
                </RouteLink>
              </Cell>
              <Cell>
                {operation.user.lastName} {operation.user.firstName}
              </Cell>
              <Cell>
                {operation.externalType === 'CARD' ? 'Card Withdrawal' : 'Bitcoin Withdrawal'}
              </Cell>
              <Cell>
                {operation.amount}
              </Cell>
              <Cell>
                {moment(new Date(operation.date)).format('DD-MM-YYYY, HH:mm')}
              </Cell>
              <Cell>
                {operation.user.balance}
              </Cell>
              <Cell>
                {statuses[operation.status]}
              </Cell>
            </TableRow>
          ))}
        </Table>
      </Layout>
    </Row>
  </Block>
)

export default External
