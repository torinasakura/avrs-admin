import React from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { TabRouteLink } from 'avrs-ui/src/link'

const Operations = ({ children }) => (
  <Row>
    <Layout>
      <Block attach='bottom'>
        <Column align='center'>
          <Layout>
            <TabRouteLink to='/operations/external'>
              Вывод
            </TabRouteLink>
          </Layout>
          <Layout>
            <TabRouteLink to='/operations/internal'>
              Между счетами
            </TabRouteLink>
          </Layout>
        </Column>
      </Block>
    </Layout>
    <Layout>
      {children}
    </Layout>
  </Row>
)

export default Operations
