import React from 'react'
import moment from 'moment'
import { Column, Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { Button, GhostButton } from 'avrs-ui/src/button'
import { Condition } from 'avrs-ui/src/condition'
import { Modal } from 'avrs-ui/src/modal'
import { Text } from 'avrs-ui/src/text'
import { Label } from 'avrs-ui/src/label'
import { Input } from 'avrs-ui/src/input'

const statuses = {
  SENT: 'Ожидание',
  PERFORMED: 'Выполнена',
  NOT_PERFORMED: 'Отклонена',
}

const ExternalDetail = ({ id, user = {}, date, externalType, amount, status, goBack, onConfirm, onCancel }) => (
  <Modal onClose={goBack}>
    <Block
      offset
      shadow
    >
      <div style={{ width: 600 }}>
        <Row>
          <Layout>
            <Column align='center'>
              <Layout>
                <Text size='large'>
                  Вывод средств #{id}
                </Text>
              </Layout>
              <Layout grow={1} />
              <Layout>
                <Text color='gray400' size='xsmall'>
                  {moment(new Date(date)).format('DD MMMM YYYY, HH:mm')}
                </Text>
              </Layout>
              <Layout basis='6px' />
              <Layout>
                <Text size='small' lineHeight='extended'>
                  {statuses[status]}
                </Text>
              </Layout>
            </Column>
          </Layout>
          <Layout basis='30px' />
          <Layout>
            <Label>
              Пользователь
            </Label>
          </Layout>
          <Layout basis='5px' />
          <Layout>
            <Input
              readOnly
              value={`${user.lastName} ${user.firstName}, € ${user.balance} `}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <Label>
              Сумма
            </Label>
          </Layout>
          <Layout basis='5px' />
          <Layout>
            <Input
              readOnly
              value={amount}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <Label>
              Тип
            </Label>
          </Layout>
          <Layout basis='5px' />
          <Layout>
            <Input
              readOnly
              value={externalType === 'CARD' ? 'Card Withdrawal' : 'Bitcoin Withdrawal'}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <Label>
              Номер
            </Label>
          </Layout>
          <Layout basis='5px' />
          <Layout>
            <Input
              readOnly
              value={externalType === 'CARD' ? user.cardNumber : user.btcAddress}
            />
          </Layout>
          <Layout basis='40px' />
          <Layout>
            <Column>
              <Layout grow={1} />
              <Condition match={status === 'SENT'}>
                <Layout>
                  <Button onClick={onConfirm}>
                    Подтвердить
                  </Button>
                </Layout>
              </Condition>
              <Layout basis='10px' />
              <Condition match={status === 'SENT'}>
                <Layout>
                  <GhostButton
                    color='blue'
                    onClick={onCancel}
                  >
                    Отменить
                  </GhostButton>
                </Layout>
              </Condition>
              <Layout basis='10px' />
              <Layout>
                <GhostButton onClick={goBack}>
                  Закрыть
                </GhostButton>
              </Layout>
            </Column>
          </Layout>
        </Row>
      </div>
    </Block>
  </Modal>
)

export default ExternalDetail
