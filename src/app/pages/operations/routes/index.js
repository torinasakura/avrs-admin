import { load } from '../actions'
import Operations from '../containers/Operations'
import External from '../containers/External'
import ExternalDetail from '../containers/ExternalDetail'
import Internal from '../containers/Internal'
import InternalDetail from '../containers/InternalDetail'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'operations',
    component: Operations,
    onEnter(nextState, replace) {
      dispatch(load())

      if (nextState.location.pathname === '/operations') {
        replace('/operations/external')
      }
    },
    childRoutes: [{
      path: 'external',
      component: External,
      childRoutes: [{
        path: ':id',
        component: ExternalDetail,
      }],
    }, {
      path: 'internal',
      component: Internal,
      childRoutes: [{
        path: ':id',
        component: InternalDetail,
      }],
    }],
  }]
}
