import { connect } from 'react-redux'
import { markAsRead } from '../actions/external'
import ExternalDetail from '../components/ExternalDetail'

export default connect(
  (state) => {
    const { id } = state.router.params
    const [request] = state.support.external.filter(item => item.id === id)

    return {
      ...request,
    }
  },
  dispatch => ({
    onMarkAsRead: id => dispatch(markAsRead(id)),
  }),
)(ExternalDetail)
