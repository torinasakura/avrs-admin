import { connect } from 'react-redux'
import { change, send } from '../actions/internal'
import InternalDetail from '../components/InternalDetail'

export default connect(
  state => state.support.internal.detail,
  dispatch => ({
    onChangeMessage: value => dispatch(change(value)),
    onSend: () => dispatch(send()),
  }),
)(InternalDetail)
