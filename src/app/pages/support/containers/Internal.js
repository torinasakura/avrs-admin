import { connect } from 'react-redux'
import Internal from '../components/Internal'

export default connect(
  state => ({
    requests: state.support.internal.list,
  }),
)(Internal)
