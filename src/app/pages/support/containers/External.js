import { connect } from 'react-redux'
import External from '../components/External'

export default connect(
  state => ({
    requests: state.support.external,
  }),
)(External)
