import gql from 'graphql-tag'
import * as actions from '../constants/external'

export function load() {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          externalSupportRequests {
            id
            status
            email
            subject
            message
            createdAt
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      requests: data.externalSupportRequests,
    })
  }
}

export function markAsRead(id) {
  return async (dispatch, getState, client) => {
    const { data } = await client.mutate({
      mutation: gql`
        mutation markAsReadExternalSupportRequest($id: ID!) {
          markAsReadExternalSupportRequest(id: $id) {
            id
            status
            email
            subject
            message
            createdAt
          }
        }
      `,
      variables: {
        id,
      },
    })

    dispatch({
      type: actions.markAsRead,
      request: data.markAsReadExternalSupportRequest,
    })
  }
}
