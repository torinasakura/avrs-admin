import gql from 'graphql-tag'
import * as actions from '../constants/internal'

export function load() {
  return async (dispatch, getState, client) => {
    const { data } = await client.query({
      query: gql`
        query {
          supportRequests {
            id
            status
            subject
            createdAt
            messages {
              id
              type
              body
              read
              createdAt
            }
          }
        }
      `,
    })

    dispatch({
      type: actions.load,
      requests: data.supportRequests,
    })
  }
}

export function loadDetail(id) {
  return async (dispatch, getState) => {
    const [request] = getState().support.internal.list.filter(item => item.id === id)

    if (request) {
      dispatch({
        type: actions.loadDetail,
        request,
      })
    }
  }
}

export function change(message) {
  return {
    type: actions.change,
    message,
  }
}

export function send() {
  return async (dispatch, getState, client) => {
    const { message, id } = getState().support.internal.detail

    if (message && message.length === 0) {
      return
    }

    const { data } = await client.mutate({
      mutation: gql`
        mutation sendSupportRequestMessage($requestId: ID!, $message: String!) {
          sendSupportRequestMessage(requestId: $requestId, message: $message) {
            errors {
              key
              message
            }
            message {
              id
              type
              body
              read
              createdAt
            }
          }
        }
      `,
      variables: {
        message,
        requestId: id,
      },
    })

    if (data.sendSupportRequestMessage.errors.length === 0) {
      dispatch({
        type: actions.addMessage,
        message: data.sendSupportRequestMessage.message,
      })
    }
  }
}
