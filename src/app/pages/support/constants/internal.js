export const load = '@@avrs-admin/support/internal/LOAD'
export const loadDetail = '@@avrs-admin/support/internal/LOAD_DETAIL'
export const change = '@@avrs-admin/support/internal/CHANGE'
export const addMessage = '@@avrs-admin/support/internal/ADD_MESSAGE'
