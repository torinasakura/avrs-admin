import { load as loadExternal } from '../actions/external'
import { load as loadInternal, loadDetail as loadInternalDetail } from '../actions/internal'
import Support from '../containers/Support'
import External from '../containers/External'
import ExternalDetail from '../containers/ExternalDetail'
import Internal from '../containers/Internal'
import InternalDetail from '../containers/InternalDetail'

export default function getRoutes({ dispatch }) {
  return [{
    path: 'support',
    component: Support,
    childRoutes: [{
      path: 'internal',
      component: Internal,
      onEnter() {
        dispatch(loadInternal())
      },
    }, {
      path: 'internal/:id',
      component: InternalDetail,
      onEnter({ params }) {
        dispatch(loadInternalDetail(params.id))
      },
    }, {
      path: 'external',
      component: External,
      onEnter() {
        dispatch(loadExternal())
      },
      childRoutes: [{
        path: ':id',
        component: ExternalDetail,
      }],
    }],
  }]
}
