import moment from 'moment'
import { createReducer } from '../../../../../utils'
import * as actions from '../../constants/internal'

const initialState = []

const formatRequest = ({ createdAt, ...request }) => ({
  ...request,
  createdAt: moment(new Date(createdAt)).format('YYYY-MM-DD, HH:mm:ss'),
})

const sortByCreatedAt = (left, right) =>
  moment.utc(new Date(right.createdAt)).diff(moment.utc(new Date(left.createdAt)))

export default createReducer(initialState, {
  [actions.load]: (state, { requests }) => requests.sort(sortByCreatedAt).map(formatRequest),
})
