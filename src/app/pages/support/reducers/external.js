import moment from 'moment'
import { createReducer } from '../../../../utils'
import * as actions from '../constants/external'

const initialState = []

const formatRequest = ({ createdAt, ...request }) => ({
  ...request,
  createdAt: moment(new Date(createdAt)).format('YYYY-MM-DD, HH:mm:ss'),
})

const groupByStatus = requests =>
  requests.reduce((result, request) => ({
    ...result,
    [request.status.toLowerCase()]: [
      ...result[request.status.toLowerCase()],
      request,
    ],
  }), { new: [], closed: [] })

const sortByCreatedAt = (left, right) =>
  moment.utc(new Date(right.createdAt)).diff(moment.utc(new Date(left.createdAt)))

const sortRequests = (requests) => {
  const groups = groupByStatus(requests)

  return groups.new.sort(sortByCreatedAt)
                   .concat(groups.closed.sort(sortByCreatedAt))
}

export default createReducer(initialState, {
  [actions.load]: (state, { requests }) => sortRequests(requests.map(formatRequest)),
  [actions.markAsRead]: (state, { request }) => state.map((item) => {
    if (item.id === request.id) {
      return {
        ...item,
        ...formatRequest(request),
      }
    }

    return item
  }),
})
