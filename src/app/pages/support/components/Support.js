import React, { PropTypes } from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { TabRouteLink } from 'avrs-ui/src/link'

const Support = ({ children }) => (
  <Row>
    <Layout>
      <Block attach='bottom'>
        <Column align='center'>
          <Layout>
            <TabRouteLink to='/support/internal'>
              Обращения
            </TabRouteLink>
          </Layout>
          <Layout>
            <TabRouteLink to='/support/external'>
              Обращения с сайта
            </TabRouteLink>
          </Layout>
        </Column>
      </Block>
    </Layout>
    <Layout>
      {children}
    </Layout>
  </Row>
)

Support.propTypes = {
  children: PropTypes.element,
}

export default Support
