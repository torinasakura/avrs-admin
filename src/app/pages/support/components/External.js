import React, { PropTypes } from 'react'
import { Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { Table, Row as TableRow, Cell } from 'avrs-ui/src/table'
import { RouteLink } from 'avrs-ui/src/link'
import { Text } from 'avrs-ui/src/text'

const External = ({ children, requests }) => (
  <Block
    offset
    shadow
  >
    <Row>
      <Layout>
        {children}
      </Layout>
      <Layout>
        <Table>
          <colgroup>
            <col style={{ width: '25%' }} />
            <col style={{ width: '15%' }} />
            <col style={{ width: '15%' }} />
            <col style={{ width: '45%' }} />
          </colgroup>
          <TableRow>
            <Cell
              align='center'
              weight='medium'
            >
              Email
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Создана
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Статус
            </Cell>
            <Cell
              align='center'
              weight='medium'
            >
              Тема
            </Cell>
          </TableRow>
          {requests.map((request, index) => (
            <TableRow key={index}>
              <Cell>
                <RouteLink to={`/support/external/${request.id}`}>
                  <Text size='small' color='blue400'>
                    {request.email}
                  </Text>
                </RouteLink>
              </Cell>
              <Cell>
                {request.createdAt}
              </Cell>
              <Cell>
                {request.status === 'NEW' ? 'Новое' : 'Закрыто'}
              </Cell>
              <Cell>
                {request.subject}
              </Cell>
            </TableRow>
          ))}
        </Table>
      </Layout>
    </Row>
  </Block>
)

External.propTypes = {
  children: PropTypes.element,
  requests: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    status: PropTypes.string,
    email: PropTypes.string,
    subject: PropTypes.string,
    createdAt: PropTypes.string,
  })),
}

export default External
