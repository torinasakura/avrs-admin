import React, { PropTypes } from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { Button, GhostButton } from 'avrs-ui/src/button'
import { Condition } from 'avrs-ui/src/condition'
import { Modal } from 'avrs-ui/src/modal'
import { Text } from 'avrs-ui/src/text'
import { Label } from 'avrs-ui/src/label'
import { Input } from 'avrs-ui/src/input'
import { Textarea } from 'avrs-ui/src/textarea'

const ExternalDetail = ({ history, id, status, email, subject, message, onMarkAsRead }) => (
  <Modal onClose={history.goBack}>
    <Block
      offset
      shadow
    >
      <div style={{ width: 600 }}>
        <Row>
          <Layout>
            <Column>
              <Layout>
                <Text size='large'>
                  Заголовок
                </Text>
              </Layout>
            </Column>
          </Layout>
          <Layout basis='30px' />
          <Layout>
            <Label>
              Email адрес
            </Label>
          </Layout>
          <Layout basis='5px' />
          <Layout>
            <Input
              readOnly
              value={email}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <Label>
              Тема обращения
            </Label>
          </Layout>
          <Layout basis='5px' />
          <Layout>
            <Input
              readOnly
              value={subject}
            />
          </Layout>
          <Layout basis='20px' />
          <Layout>
            <Label>
              Сообщение
            </Label>
          </Layout>
          <Layout basis='5px' />
          <Layout>
            <Textarea
              readOnly
              rows={8}
              value={message}
            />
          </Layout>
          <Layout basis='40px' />
          <Layout>
            <Column>
              <Layout grow={1} />
              <Layout>
                <Condition match={status === 'NEW'}>
                  <Button onClick={() => onMarkAsRead(id)}>
                    Отметить как прочитанное
                  </Button>
                </Condition>
              </Layout>
              <Layout basis='10px' />
              <Layout>
                <GhostButton
                  color='blue'
                  onClick={history.goBack}
                >
                  Закрыть
                </GhostButton>
              </Layout>
            </Column>
          </Layout>
        </Row>
      </div>
    </Block>
  </Modal>
)

ExternalDetail.propTypes = {
  history: PropTypes.object,
  id: PropTypes.string,
  status: PropTypes.string,
  email: PropTypes.string,
  subject: PropTypes.string,
  message: PropTypes.string,
  onMarkAsRead: PropTypes.func,
}

export default ExternalDetail
