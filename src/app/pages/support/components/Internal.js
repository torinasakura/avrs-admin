import React, { PropTypes } from 'react'
import { Column, Row, Layout } from 'flex-layouts'
import { Block } from 'avrs-ui/src/content'
import { Table, Row as TableRow, Cell } from 'avrs-ui/src/table'
import { RouteLink } from 'avrs-ui/src/link'
import { Text } from 'avrs-ui/src/text'
import Status from 'avrs-cabinet/src/app/pages/support/components/Status'
import Unread from 'avrs-cabinet/src/app/pages/support/components/Unread'

const Internal = ({ children, requests = [] }) => (
  <Block
    offset
    shadow
  >
    <Row>
      <Layout>
        {children}
      </Layout>
      <Layout>
        <Table>
          <colgroup>
            <col style={{ width: '5%' }} />
            <col style={{ width: '65%' }} />
            <col style={{ width: '15%' }} />
            <col style={{ width: '15%' }} />
          </colgroup>
          <TableRow>
            <Cell>
              #
            </Cell>
            <Cell>
              Тема
            </Cell>
            <Cell>
              Дата
            </Cell>
            <Cell>
              Статус
            </Cell>
          </TableRow>
          {requests.map(request => (
            <TableRow key={request.id}>
              <Cell>
                {request.id}
              </Cell>
              <Cell>
                <Column align='center'>
                  <Layout grow={1}>
                    <RouteLink to={`/support/internal/${request.id}`}>
                      <Text
                        size='small'
                        color='gray400'
                      >
                        {request.subject}
                      </Text>
                    </RouteLink>
                  </Layout>
                  <Layout basis='10px' />
                  <Layout>
                    <Unread messages={request.unread} />
                  </Layout>
                </Column>
              </Cell>
              <Cell>
                {request.createdAt}
              </Cell>
              <Cell>
                <Status>
                  {request.status}
                </Status>
              </Cell>
            </TableRow>
          ))}
        </Table>
      </Layout>
    </Row>
  </Block>
)

Internal.propTypes = {
  children: PropTypes.element,
  requests: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    status: PropTypes.string,
    email: PropTypes.string,
    subject: PropTypes.string,
    createdAt: PropTypes.string,
  })),
}

export default Internal
