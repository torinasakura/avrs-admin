import { connect } from 'react-redux'
import { load, logout } from '../actions/user'
import App from '../components/App'

export default connect(
  state => ({
    firstName: state.user.firstName,
    lastName: state.user.lastName,
  }),
  dispatch => ({
    onLoad: () => dispatch(load()),
    onLogout: () => dispatch(logout()),
  }),
)(App)
