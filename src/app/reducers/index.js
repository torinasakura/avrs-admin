import { compose, combineReducers } from 'redux'
import { routerStateReducer as router } from 'redux-router'
import { mergePersistedState } from 'redux-localstorage'
import client from '../api/client'
import user from './user'
import remote from './remote'
import auth from '../pages/auth/reducers'
import support from '../pages/support/reducers'
import users from '../pages/users/reducers'
import operations from '../pages/operations/reducers'

const reducers = combineReducers({
  apollo: client.reducer(),
  router,
  user,
  remote,
  auth,
  support,
  users,
  operations,
})

export default compose(
  mergePersistedState(),
)(reducers)
