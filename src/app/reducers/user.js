import { createReducer } from '../../utils'
import * as actions from '../constants/user'

const initialState = {
  token: undefined,
  balance: 0,
  activations: [],
}

export default createReducer(initialState, {
  [actions.auth]: (state, { user }) => user,
  [actions.logout]: () => initialState,
  [actions.load]: (state, { user }) => ({ ...state, ...user }),
})
