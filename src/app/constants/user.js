export const auth = '@@avrs-admin/user/AUTH'
export const load = '@@avrs-admin/user/LOAD'
export const logout = '@@avrs-admin/user/LOGOUT'
